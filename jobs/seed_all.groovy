job('Seed All') {
    scm {
        git ('https://gitlab.com/singnaja/dsl-job.git')
    }
    steps {
        dsl {
            external('jobs/*.groovy')
            // default behavior
            // removeAction('IGNORE')
            removeAction('DELETE')
        }
    }
}